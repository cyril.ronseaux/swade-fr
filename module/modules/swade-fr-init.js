
/************************************************************************************/
Hooks.once('init', () => {

  if(typeof Babele !== 'undefined') {
		
    console.log("BABELE LOADED !!!");
		Babele.get().register({
			module: 'swade-fr',
			lang: 'fr',
			dir: 'compendiums'
		});

    Babele.get().registerConverters({
      "powers_duration": (duration) => {
        if ( duration == 'One hour') return 'Une heure';
        if ( duration == 'Instant (slot); 5 (speed)') return 'Instantané (apathie); 5 (vitesse)';
        if ( duration == '(boost); Instant (lower)') return '5 (augmentation); Instantané (diminution)';
        if ( duration == 'Instant') return 'Instantanné';
        if ( duration == 'Until the end of the victim\'s next turn') return 'Jusqu`à la fin du prochain tour de la cible';
        if ( duration == 'A brief conversation of about five minutes') return 'Une brève conversation d\'environ 5 minutes';
        if ( duration == '5 (detect), one hour (conceal)') return '5 (détection); une heure (dissimulation)';
        if ( duration == 'Instant (Sound); 5 (Silence)') return 'Instantané (Son); 5 (Silence)';
        return duration;
      },
      "powers_range": (range) => {
        if ( range == 'Smarts x5 (Sound); Smarts (Silence)') 
          return 'Intellect x5 (Son); Intellect (Silence)';
        if ( range == "Cone Template")
          return "Cône"
        if ( range == "Touch")
          return "Toucher"
        if ( range == "Sm")
          return "Ame mètres"
        if ( range == "Sm x 2" || range == "Sm x2")
          return "Ame mètres x 2"
        if ( range == "Self")
          return "Soi-même"
        return range;
      },
      "powers_rank": (rank) => {
        if ( rank == 'Novice') return 'Novice';
        if ( rank == 'Seasoned') return 'Aguerri';
        if ( rank == 'Veteran') return 'Vétéran';
        if ( rank == 'Heroic') return 'Héroïque';
        return rank;
      }    
    } );
  }

});

/************************************************************************************/
const transFolder = { "Actor": "Acteur", "Edges": "Atouts"}
const subFolder = { 'Social Edges': 'Atouts sociaux', 'Background Edges': 'Atouts de Background', 'Combat Edges': 'Atouts de combat', 
'Leadership Edges': 'Atouts de commandement', 'Legendary Edges': 'Atouts légendaires', 'Power Edges': 'Atouts de pouvoir', 
'Professional Edges': 'Atouts professionnels', 'Weird Edges': 'Atouts étranges', 'Edges': 'Atouts', 'Hindrances': 'Handicaps', 'Skills': 'Compétences', 
'Equipment': 'Equipement', 'Ammo': 'Munitions', 'Armor': 'Armure', 'Common Gear': 'Matériel commun', 'Modern Firearms': 'Armes à feu modernes', 
'Personal Weapons': 'Armes', 'Special Weapons': 'Armes spéciales', 'Bullet': 'Balles', 'Cannon': 'Canon', 'Laser Battery': 'Batterie Laser',
'Adventuring Gear': 'Matériel d\'aventure', 'Animals and Tack': 'Animaux', 'Clothing': 'Vêtements', 'Computers & Electronics':'Ordinateurs et Electroniques',
'Firearms Accessories': 'Accessoires armes à feu', 'Food': 'Nourriture', 'Personal Defense': 'Auto-défense', 'Futuristic': 'Futuriste', 
'Medieval & Ancient': 'Médiévale et Antiquité', 'Modern': 'Moderne', 'Shields': 'Boucliers', 'Laser (Futuristic)': 'Laser (Fururiste)',
'Machine Guns': 'Mitraillettes', 'Pistols': 'Pistolets', 'Rifles': 'Fusils', 'Submachine Guns': 'Semi-automatiques', 'Cannons': 'Canons', 
'Catapults': 'Catapultes', 'Flamethrowers': 'Lance-flammes', 'Rocket Launchers & Torpedoes': 'Lance roquettes et torpilles', 
'Vehicular Weapons': 'Armes de véhicules'  }

/************************************************************************************/
const __translate_sub_folders =  async (packName) => {
  let pack = game.packs.get(packName)
  pack.locked = false;
  let folderEntityIds = pack.index.filter(x => x.name === "#[CF_tempEntity]");
  for ( let index of folderEntityIds) {
    let folderEntity = await pack.getEntity(index._id);
    //console.log("Fold1", folderEntity);
    let newName = subFolder[folderEntity.data.flags.cf.name];
    if ( newName) {
      await pack.updateEntity({ _id: index._id, 'flags.cf.name': newName } );
    }
  }    
}

/************************************************************************************/
const __fix_folder_names = async ( ) =>  {
  if ( game.customFolders.compendium.folders ) {
    for ( let [key, trans] in transFolder) {
      for ( let folder of game.customFolders.compendium.folders.entries) {
        folder.name = trans;
        folder.save(false);
      }
    }
  }

  __translate_sub_folders("swade-core-rules.swade-edges");
  __translate_sub_folders("swade-core-rules.swade-hindrances");
  __translate_sub_folders("swade-core-rules.swade-equipment");

}

/************************************************************************************/
Hooks.once('ready', () => {
  if (game.user.isGM) {
    setTimeout( __fix_folder_names, 4000, true, false);
  }

});
